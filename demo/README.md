# SWA Bazaar UserService


## How to run

after cloning repository run following commands in shell

`docker compose pull`
`docker compose up -d`

## Swagger
http://localhost:8088/swagger-ui/index.html#/

## Ports
- :5431 - Postgres DB
- :8088 - Bazar service

## Docker Hub

https://hub.docker.com/repository/docker/blobaa791d/demo

## Interesting Links

- [Deploying Image to Docker Hub](https://dev.to/mattdark/publish-a-docker-image-to-docker-hub-using-gitlab-2b9o)
- https://docs.docker.com/compose/reference/