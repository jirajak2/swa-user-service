package com.example.demo.user;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping(path = "api/v1/user")
@Api(tags="User Controller")
public class UsersController {

    private final UsersService usersService;

    @Autowired
    public UsersController(UsersService usersService) {
        this.usersService = usersService;
    }

    @ApiOperation(value = "Get all users", notes = "Returns all users filtered by parameters used below. Use none or one filter")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Command successful"),
            @ApiResponse(code = 400, message = "Bad request")
    })
    @GetMapping(produces = "application/json")
    public List<Users> getUsers(
            @RequestParam(value = "fullname", required = false) String fullName,
            @RequestParam(value = "username", required = false) String userName,
            @RequestParam(value = "email", required = false) String email
    ){
        return usersService.getUsers(fullName, userName, email);
    }
    @ApiOperation(value = "Get user with specific id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "User successfully found"),
            @ApiResponse(code = 404, message = "Not found - The user was not found in database")
    })
    @GetMapping(path = "{userId}")
    public Users getUser(@PathVariable("userId") Long id){
        return usersService.getUser(id);
    }

    @ApiOperation(value = "Create a new user", notes = "Required parameters are email, fullName and dateOfBirth.\n" +
            "Age is calculated based on dateOfBirth, id is generated by database and empty username will use email as username")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "User successfully created in database"),
            @ApiResponse(code = 400, message = "Bad request, user missing some crucial information"),
            @ApiResponse(code = 409, message = "Conflict, user with same email is already present in database")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(
                    name = "user",
                    dataTypeClass = Users.class,
                    example = "{ \"id\": 100, \"fullName\": \"Pavel Slavík\", \"email\": \"pavel@domain.com\", \"userName\": \"Paja007\", \"dateOfBirth\": \"1983-04-15\"}"
            )
    })
    @PostMapping
    public Users newUser(@RequestBody Users user){
        return usersService.addNewUsers(user);
    }

    @ApiOperation(value = "Put information into user", notes = "Requires userID\n" +
            "userName will not be nulled if null userName will be given. This will not raise any error.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "User successfully updated in database"),
            @ApiResponse(code = 400, message = "Bad request: user missing ID"),
            @ApiResponse(code = 404, message = "Not found: given ID not found in database"),
            @ApiResponse(code = 409, message = "Conflict:  user with same email or userName is already present in database")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(
                    name = "user",
                    dataTypeClass = Users.class,
                    example = "{ \"id\": 100, \"fullName\": \"Pavel Slavík\", \"email\": \"pavel@domain.com\", \"userName\": \"Paja007\", \"dateOfBirth\": \"1983-04-15\"}"
            )
    })
    @PutMapping
    public Users putToUser(@RequestBody Users user){
        return usersService.putToUser(user);
    }

    @ApiOperation(value = "Delete an user by id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "User successfully removed from database"),
            @ApiResponse(code = 404, message = "Not found - The user was not found in database")
    })
    @DeleteMapping(path = "{userID}")
    public void deleteUser(@PathVariable("userID") Long userId){
        usersService.deleteUser(userId);
    }


    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "User has been updated"),
            @ApiResponse(code = 400, message = "Update missing some crucial information"),
            @ApiResponse(code = 404, message = "User not found"),
            @ApiResponse(code = 409, message = "Targeted mail or username already present in database")
    })
    @ApiOperation(value = "Update credentials of user specified by id", notes = "Enter dateOfBirth in yyyy-mm-dd format")
    @PatchMapping(path = "{userId}", produces = "application/json")
    public Users updateUsers(@PathVariable("userId") @ApiParam(value = "User id", example = "1", required = true) Long id,
                            @RequestParam(required = false) String fullname,
                            @RequestParam(required = false) String email,
                            @RequestParam(required = false) String dateOfBirth,
                            @RequestParam(required = false) String username
                            ) {
        return usersService.updateUsers(id, fullname, email, username, dateOfBirth);
    }
}
