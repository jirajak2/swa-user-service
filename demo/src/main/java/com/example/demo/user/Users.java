package com.example.demo.user;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.Period;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;

@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Entity
@Table
public class Users {
    @Id
    @SequenceGenerator(
            name = "user_sequence",
            sequenceName = "user_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "user_sequence"
    )
    @ApiModelProperty(notes = "UserID", example = "11", required = true)
    private Long id;
    @ApiModelProperty(notes = "Full Name", example = "Pavel Slavík", required = true)
    private String fullName;
    @Transient
    private Integer age;
    @ApiModelProperty(notes = "Date of birth", example = "1998-11-08", required = true)
    private LocalDate dateOfBirth;
    @ApiModelProperty(notes = "Email", example = "pavel@domain.com", required = true)
    private String email;
    @ApiModelProperty(notes = "UserName", example = "Pavlik007", required = true)
    private String userName;

    public Users() {
    }

    public Users(Long id, String name, String email, String userName, LocalDate dateOfBirth) {
        this.id = id;
        this.fullName = name;
        this.email = email;
        this.userName = userName;
        this.dateOfBirth = dateOfBirth;
    }
    public Users(Long id, String name, String email, LocalDate dateOfBirth) {
        this.id = id;
        this.fullName = name;
        this.email = email;
        this.userName = email;
        this.dateOfBirth = dateOfBirth;
    }

    public Users(String name, String email, String userName, LocalDate dateOfBirth) {
        this.fullName = name;
        this.dateOfBirth = dateOfBirth;
        this.email = email;
        this.userName = userName;
    }
//
//    public Users(String name, String email, LocalDate dateOfBirth) {
//        this.fullName = name;
//        this.dateOfBirth = dateOfBirth;
//        this.email = email;
//        this.userName = email;
//    }
    public Users(String userName){
        this.userName = userName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String name) {
        this.fullName = name;
    }

    public Integer getAge() {
        if(this.dateOfBirth == null){
            return null;
        }
        return Period.between(dateOfBirth, LocalDate.now()).getYears();
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dob) {
        this.dateOfBirth = dob;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void fromUser(Users user){
        this.setEmail(user.getEmail());
        if (user.getUserName() != null) {
            this.setUserName(user.getUserName());
        }
        this.setDateOfBirth(user.getDateOfBirth());
        this.setFullName(user.getFullName());
    }

    @Override
    public String toString() {
        if (fullName == null || dateOfBirth == null || email == null) {
            return "Users{" +
                    "id=" + id +
                    ", userName='" + userName + '\'' +
                    '}';
        }
        return "Users{" +
                "id=" + id +
                ", fullName='" + fullName + '\'' +
                ", age=" + this.getAge() +
                ", dateOfBirth=" + dateOfBirth +
                ", email='" + email + '\'' +
                ", userName='" + userName + '\'' +
                '}';
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Users)) return false;
        Users users = (Users) o;
        return id.equals(users.id) && Objects.equals(fullName, users.fullName) && Objects.equals(dateOfBirth, users.dateOfBirth) && Objects.equals(email, users.email) && userName.equals(users.userName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, fullName, dateOfBirth, email, userName);
    }
}
