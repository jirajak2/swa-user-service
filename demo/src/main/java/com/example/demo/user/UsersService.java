package com.example.demo.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Objects;

@Service
public class UsersService {

    private final UsersRepository usersRepository;

    @Autowired
    public UsersService(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    public List<Users> getUsers(String fullName, String userName, String email){
        if (userName == null && email == null && fullName == null) {
            return usersRepository.findAll();
        } else if (email != null) {
             return usersRepository.findUsersByEmail(email);
        } else if (fullName != null) {
            return usersRepository.findUsersByFullName(fullName);
        }
        return usersRepository.findUsersByUserName(userName);
    }

//    public List<Users> getUserByEmail(String email){
//        List<Users> user = usersRepository.findUsersByEmail(email);
//        if ( user.isEmpty()){
//            throw new IllegalStateException("user with email " + email + " not found in database");
//        }
//        return user;
//    }

    public Users getUser(Long id){
        boolean exists = usersRepository.existsById(id);
        if (!exists) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Not found: user with id " + id + " does not exist");
        }
        return usersRepository.getById(id);
    }

    public Users addNewUsers(Users user) {
        if(user.getEmail() == null){
        } else {
            List<Users> usersOptional =  usersRepository.findUsersByEmail(user.getEmail());
            if (!usersOptional.isEmpty()){
                throw new ResponseStatusException(HttpStatus.CONFLICT, "Conflict: email " + user.getEmail() + " found in database");
            }
        }
        if(user.getFullName() == null){
            user.setFullName(null);
        }
        if(user.getUserName() == null){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Bad request: Missing username");
        } else {
            if (!usersRepository.findUsersByUserName(user.getUserName()).isEmpty()) {
                throw new ResponseStatusException(HttpStatus.CONFLICT, "Conflict: username " + user.getUserName() + " found in database");
            }
        }
        if(user.getDateOfBirth() == null){

        }
        usersRepository.save(user);
        return user;
    }
    @Transactional
    public Users putToUser(Users user) {
        if (user.getId() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Bad request: given id is null");
        }
        if (!usersRepository.existsById(user.getId())){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Not found: user with id " + user.getId() + " not found");
        }
        Users u = usersRepository.getById(user.getId());
        if (user.getEmail() != null) {
            List<Users> usersOptional =  usersRepository.findUsersByEmail(user.getEmail());
            if (!usersOptional.isEmpty()){
                throw new ResponseStatusException(HttpStatus.CONFLICT, "Conflict: email " + user.getEmail() + " already present in database");
            }
        }
        if(user.getUserName() != null){
            if (!usersRepository.findUsersByUserName(user.getUserName()).isEmpty()) {
                throw new ResponseStatusException(HttpStatus.CONFLICT, "Conflict: username " + user.getUserName() + " already present in database");
            }
        }
        u.fromUser(user);
        return u;
    }
    public void deleteUser(Long id) {
        //usersRepository.findById(id);
        boolean exists = usersRepository.existsById(id);
        if ( !exists ) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Not found: user of id: " + id + " not found in database");
        }
        usersRepository.deleteById(id);
    }
    @Transactional
    public Users updateUsers(Long id, String name, String email, String username, String dateOfBirth) {
        if ( !usersRepository.existsById(id) ) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Not found: user with id " + id + " was not found in database");
        }
        Users user = usersRepository.getById(id);

        if ( name != null && name.length() > 0 && !Objects.equals(user.getFullName(), name) ) {
            user.setFullName(name);
        }
        if ( email != null && email.length() > 0 && !Objects.equals(user.getEmail(), email) ) {
            List<Users> usersOptional = usersRepository.findUsersByEmail(email);
            if (!usersOptional.isEmpty()) {
                throw new ResponseStatusException(HttpStatus.CONFLICT, "Conflict: email " + email + " is already in use by another user");
            }
            user.setEmail(email);
        }
        if ( username != null && username.length() > 0 && !Objects.equals(user.getUserName(), username) ) {
            user.setUserName(username);
        }
        if ( dateOfBirth != null && !Objects.equals(user.getDateOfBirth().toString(), dateOfBirth) ) {
            try {
                user.setDateOfBirth(LocalDate.parse(dateOfBirth));
            } catch (DateTimeParseException e) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Bad request: date " + dateOfBirth + " is not a valid date");
            }
        }
        return user;
    }
}
