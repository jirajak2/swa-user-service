package com.example.demo.user;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

@Configuration
public class UsersConfig {

    @Bean
    CommandLineRunner commandLineRunner(UsersRepository repository) {
        return args -> {
            Users blob = new Users(
                    "blob",
                    "protonmail@@@.com",
                    "blobasek19",
                    LocalDate.of(2012, Month.APRIL, 4)

            );
            Users alex = new Users(
                    "alex",
                    "alex@rixo.com",
                    "alex@rixo.com",
                    LocalDate.of(1990, Month.APRIL, 24)
            );
            Users ann = new Users(
                    "Anna Urbánková",
                    "anna.urbankova@rixo.com",
                    "annurban17",
                    LocalDate.of(1999, Month.FEBRUARY, 26)
            );

            repository.saveAll(List.of(blob, alex, ann));
        };
    }
}
