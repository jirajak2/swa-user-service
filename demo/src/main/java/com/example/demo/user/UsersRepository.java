package com.example.demo.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UsersRepository extends JpaRepository<Users, Long> {

//    @Query("SELECT u FROM Users u WHERE u.email = ?1")
    List <Users> findUsersByEmail(String email);

    List <Users> findUsersByUserName(String userName);

    List <Users> findUsersByFullName(String fullName);
}
