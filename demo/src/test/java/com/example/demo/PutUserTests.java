package com.example.demo;

import com.example.demo.user.Users;
import com.example.demo.user.UsersService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class PutUserTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UsersService usersService;

    @Test
    public void putUserValid() throws Exception {
        Users newUser = new Users("Test_9999123123");
        Long newID = usersService.addNewUsers(newUser).getId();
        String json = "{ \"email\": \"test@mail.com\", \"fullName\": \"Testovaci Uzivatel\", \"dateOfBirth\": \"1990-12-12\", \"id\": " + newID + ", \"userName\": \"Pavel011\"}";
        MvcResult result = mockMvc.perform(
                        put("/api/v1/user")
                                .header("Content-Type", "application/json")
                                .content(json)
                )
                .andExpect(status().isOk())
                .andReturn();
        Users manuel = new Users(newID, "Testovaci Uzivatel", "test@mail.com", "Pavel011", LocalDate.parse("1990-12-12"));
        //System.out.println(manuel.hashCode() + " " + usersService.getUser(newID).hashCode());
        assertEquals(manuel.hashCode(), usersService.getUser(newID).hashCode());
    }
    @Test
    public void putUserBadId() throws Exception {
        Users newUser = new Users("Test_9999123124");
        Long newID = usersService.addNewUsers(newUser).getId();
        String json = "{ \"email\": \"test@mail.com\", \"fullName\": \"Testovaci Uzivatel\", \"dateOfBirth\": \"1990-12-12\", \"id\": " + newID+1 + ", \"userName\": \"Pavel011\"}";
        MvcResult result = mockMvc.perform(
                        put("/api/v1/user")
                                .header("Content-Type", "application/json")
                                .content(json)
                )
                .andExpect(status().isNotFound())
                .andReturn();
//        Users manuel = new Users(newID, "Testovaci Uzivatel", "test@mail.com", "Pavel011", LocalDate.parse("1990-12-12"));
//        assertEquals(manuel.hashCode(), usersService.getUser(newID).hashCode());
    }
    @Test
    public void putUserWithoutEmail() throws Exception {
        Users newUser = new Users("Test_99991231235");
        Long newID = usersService.addNewUsers(newUser).getId();
        //String json = "{ \"id\": " + newID + ", \"userName\": \"Pavelos000\"}";
        //String json = "{\"id\": "+newID+",  \"userName\": \"Pavlik007\"}";
        String json = "{ \"fullName\": \"Testovaci Uzivatel\", \"dateOfBirth\": \"1990-12-12\", \"id\": "+ newID +", \"userName\": \"Pavel0112\"}";

        MvcResult result = mockMvc.perform(
                        put("/api/v1/user")
                                .header("Content-Type", "application/json")
                                .content(json)
                )
                .andExpect(status().isOk())
                .andReturn();
        Users manuel = new Users(newID, "Testovaci Uzivatel", null, "Pavel0112", LocalDate.parse("1990-12-12"));
        //System.out.println(manuel.hashCode() + " " + usersService.getUser(newID).hashCode());
        assertEquals(manuel.hashCode(), usersService.getUser(newID).hashCode());
    }
    @Test
    public void putUserWithoutEmailFullName() throws Exception {
        Users newUser = new Users("Test_99991231236");
        Long newID = usersService.addNewUsers(newUser).getId();
        //String json = "{ \"id\": " + newID + ", \"userName\": \"Pavelos000\"}";
        //String json = "{\"id\": "+newID+",  \"userName\": \"Pavlik007\"}";
        String json = "{ \"dateOfBirth\": \"1990-12-12\", \"id\": "+ newID +", \"userName\": \"Pavel0113\"}";

        MvcResult result = mockMvc.perform(
                        put("/api/v1/user")
                                .header("Content-Type", "application/json")
                                .content(json)
                )
                .andExpect(status().isOk())
                .andReturn();
        Users manuel = new Users(newID, null, null, "Pavel0113", LocalDate.parse("1990-12-12"));
        //System.out.println(manuel.hashCode() + " " + usersService.getUser(newID).hashCode());
        assertEquals(manuel.hashCode(), usersService.getUser(newID).hashCode());
    }
    @Test
    public void putUserWithoutEmailFullNameDateOfBirth() throws Exception {
        Users newUser = new Users("Test_99991231237");
        Long newID = usersService.addNewUsers(newUser).getId();
        //String json = "{ \"id\": " + newID + ", \"userName\": \"Pavelos000\"}";
        //String json = "{\"id\": "+newID+",  \"userName\": \"Pavlik007\"}";
        String json = "{\"email\": \"test@mail2.com\", \"id\": "+ newID +", \"userName\": \"Pavel0114\"}";

        MvcResult result = mockMvc.perform(
                        put("/api/v1/user")
                                .header("Content-Type", "application/json")
                                .content(json)
                )
                .andExpect(status().isOk())
                .andReturn();
        Users manuel = new Users(newID, null, "test@mail2.com", "Pavel0114", null);
        //System.out.println(manuel.hashCode() + " " + usersService.getUser(newID).hashCode());
        assertEquals(manuel.hashCode(), usersService.getUser(newID).hashCode());
    }
    @Test
    public void putUserWithoutDateOfBirth() throws Exception {
        Users newUser = new Users("Test_99991231238");
        Long newID = usersService.addNewUsers(newUser).getId();
        String json = "{ \"email\": \"test@mail1.com\", \"fullName\": \"Testovaci Uzivatel\", \"id\": " + newID + ", \"userName\": \"Pavel0115\"}";
        MvcResult result = mockMvc.perform(
                        put("/api/v1/user")
                                .header("Content-Type", "application/json")
                                .content(json)
                )
                .andExpect(status().isOk())
                .andReturn();
        Users manuel = new Users(newID, "Testovaci Uzivatel", "test@mail1.com", "Pavel0115", null);
        //System.out.println(manuel.hashCode() + " " + usersService.getUser(newID).hashCode());
        assertEquals(manuel.hashCode(), usersService.getUser(newID).hashCode());
    }
    @Test
    public void putUserTestConflictUsername() throws Exception {
        Users newUser = new Users("Test_Pavel");
        Long newID = usersService.addNewUsers(newUser).getId();
        newUser = new Users("Test_99991231240");
        newID = usersService.addNewUsers(newUser).getId();
        String json = "{ \"id\": " + newID + ", \"userName\": \"Test_Pavel\"}";
        MvcResult result = mockMvc.perform(
                        put("/api/v1/user")
                                .header("Content-Type", "application/json")
                                .content(json)
                )
                .andExpect(status().isConflict())
                .andReturn();
    }
    @Test
    public void putUserTestConflictEmail() throws Exception {
        Users newUser = new Users("Test_999912312401");
        newUser.setEmail("testMail1234@gomain.com");
        Long newID = usersService.addNewUsers(newUser).getId();
        newUser = new Users("Test_999912312402");
        newID = usersService.addNewUsers(newUser).getId();
        String json = "{ \"id\": " + newID + ", \"email\": \"testMail1234@gomain.com\"}";
        MvcResult result = mockMvc.perform(
                        put("/api/v1/user")
                                .header("Content-Type", "application/json")
                                .content(json)
                )
                .andExpect(status().isConflict())
                .andReturn();
    }
}
