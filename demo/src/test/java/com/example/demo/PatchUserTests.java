package com.example.demo;

import com.example.demo.user.Users;
import com.example.demo.user.UsersService;
import org.apache.tomcat.jni.Local;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class PatchUserTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UsersService usersService;

    @Test
    public void postToValidUser() throws Exception {
        Long userToBeModifiedId = 1L;
        MvcResult result = mockMvc.perform(
                        patch("/api/v1/user/" + userToBeModifiedId)
                                .header("Content-Type", "application/json")
                                .param("email", "new@email1.com")
                                //.content(json)
                )
                .andExpect(status().isOk())
                .andReturn();
        //int numberOfItemsAfter = usersService.getUsers(null, null, null).size();

        assertEquals("new@email1.com", usersService.getUser(userToBeModifiedId).getEmail());
    }
    @Test
    public void postToInvalidUser() throws Exception {
        Long userToBeModifiedId = 1000L;
        String json = "{ \"email\": \"new@email1.com\"}";
        MvcResult result = mockMvc.perform(
                        patch("/api/v1/user/" + userToBeModifiedId)
                                .header("Content-Type", "application/json")
                                .param("email", "new@email1.com")
                )
                .andExpect(status().isNotFound())
                .andReturn();
    }
    @Test
    public void postFullNameToValidUser() throws Exception {
        Long userToBeModifiedId = 1L;
        MvcResult result = mockMvc.perform(
                        patch("/api/v1/user/" + userToBeModifiedId)
                                .header("Content-Type", "application/json")
                                .param("fullname", "pepík")
                        //.content(json)
                )
                .andExpect(status().isOk())
                .andReturn();
        //int numberOfItemsAfter = usersService.getUsers(null, null, null).size();

        assertEquals("pepík", usersService.getUser(userToBeModifiedId).getFullName());
    }
    @Test
    public void postUserNameToValidUser() throws Exception {
        Long userToBeModifiedId = 1L;
        MvcResult result = mockMvc.perform(
                        patch("/api/v1/user/" + userToBeModifiedId)
                                .header("Content-Type", "application/json")
                                .param("username", "pepík007")
                        //.content(json)
                )
                .andExpect(status().isOk())
                .andReturn();
        //int numberOfItemsAfter = usersService.getUsers(null, null, null).size();

        assertEquals("pepík007", usersService.getUser(userToBeModifiedId).getUserName());
    }
    @Test
    public void postDateOfBirthToValidUser() throws Exception {
        Long userToBeModifiedId = 1L;
        MvcResult result = mockMvc.perform(
                        patch("/api/v1/user/" + userToBeModifiedId)
                                .header("Content-Type", "application/json")
                                .param("dateOfBirth", "1999-02-26")
                        //.content(json)
                )
                .andExpect(status().isOk())
                .andReturn();
        //int numberOfItemsAfter = usersService.getUsers(null, null, null).size();

        assertEquals("1999-02-26", usersService.getUser(userToBeModifiedId).getDateOfBirth().toString());
    }
    public void postInvalidDate() throws Exception {
        Long userToBeModifiedId = 1L;
        LocalDate original = usersService.getUser(userToBeModifiedId).getDateOfBirth();
        MvcResult result = mockMvc.perform(
                        patch("/api/v1/user/" + userToBeModifiedId)
                                .header("Content-Type", "application/json")
                                .param("dateOfBirth", "1999-02-33")
                        //.content(json)
                )
                .andExpect(status().isBadRequest())
                .andReturn();
        //int numberOfItemsAfter = usersService.getUsers(null, null, null).size();

        assertEquals(original, usersService.getUser(userToBeModifiedId).getDateOfBirth());
    }
    public void postUsedEmail() throws Exception {
        Long userToBeModifiedId = 1L;
        Users u = new Users(
                "Jakub Jíra",
                "jakub@jira.com",
                "blobAA791D",
                LocalDate.of(2012, Month.APRIL, 4)
        );
        usersService.addNewUsers(u);
        String original = usersService.getUser(userToBeModifiedId).getEmail();
        MvcResult result = mockMvc.perform(
                        patch("/api/v1/user/" + userToBeModifiedId)
                                .header("Content-Type", "application/json")
                                .param("email", "jakub@jira.com")
                        //.content(json)
                )
                .andExpect(status().isConflict())
                .andReturn();
        //int numberOfItemsAfter = usersService.getUsers(null, null, null).size();

        assertEquals(original, usersService.getUser(userToBeModifiedId).getEmail());
    }
    public void postUsedUserName() throws Exception {
        Long userToBeModifiedId = 1L;
        Users u = new Users(
                "Jakub Jíra",
                "jakub@jira.com",
                "blobAA791D",
                LocalDate.of(2012, Month.APRIL, 4)
        );
        usersService.addNewUsers(u);
        String original = usersService.getUser(userToBeModifiedId).getUserName();
        MvcResult result = mockMvc.perform(
                        patch("/api/v1/user/" + userToBeModifiedId)
                                .header("Content-Type", "application/json")
                                .param("userName", "blobAA791D")
                        //.content(json)
                )
                .andExpect(status().isConflict())
                .andReturn();
        //int numberOfItemsAfter = usersService.getUsers(null, null, null).size();

        assertEquals(original, usersService.getUser(userToBeModifiedId).getUserName());
    }
}
