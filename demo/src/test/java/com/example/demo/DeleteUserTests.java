package com.example.demo;

import com.example.demo.user.Users;
import com.example.demo.user.UsersService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class DeleteUserTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UsersService usersService;

    @Test
    public void deleteUser(){
        Users u = new Users("Bramborák", "email@domain.com", "Bramborak", LocalDate.of(2002, Month.MAY, 4));
        long id = usersService.addNewUsers(u).getId();

        int count = usersService.getUsers(null, null, null).size();
        usersService.deleteUser(id);
        assertThrows(ResponseStatusException.class, () -> {
            usersService.getUser(id);
        });
        int countAfter = usersService.getUsers(null, null, null).size();
        assertEquals(count - 1, countAfter);
    }
    @Test
    public void deleteItemNotExists(){
        int count = usersService.getUsers(null, null, null).size();
        assertThrows(ResponseStatusException.class, () -> {
            usersService.deleteUser(Long.MAX_VALUE);
        });
        int countAfter = usersService.getUsers(null, null, null).size();
        assertEquals(count, countAfter);

    }
}
