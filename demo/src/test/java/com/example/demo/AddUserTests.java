package com.example.demo;

import com.example.demo.user.UsersService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class AddUserTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UsersService usersService;

    @Test
    public void addUserValid() throws Exception {
        String json = "{ \"email\": \"test@mail.com\", \"fullName\": \"Testovaci Uzivatel\", \"dateOfBirth\": \"1990-12-12\", \"id\": 10, \"userName\": \"Pavel011\"}";
        int numberOfItems = usersService.getUsers(null, null, null).size();
        MvcResult result = mockMvc.perform(
                        post("/api/v1/user")
                                .header("Content-Type", "application/json")
                                .content(json)
                )
                .andExpect(status().isOk())
                .andReturn();
        int numberOfItemsAfter = usersService.getUsers(null, null, null).size();
        assertEquals(numberOfItems + 1, numberOfItemsAfter);
    }
    @Test
    public void addUserValidWithoutProperties() throws Exception {
        String json = "{ \"userName\": \"Pavel000\"}";
        int numberOfItems = usersService.getUsers(null, null, null).size();
        MvcResult result = mockMvc.perform(
                        post("/api/v1/user")
                                .header("Content-Type", "application/json")
                                .content(json)
                )
                .andExpect(status().isOk())
                .andReturn();
        int numberOfItemsAfter = usersService.getUsers(null, null, null).size();
        assertEquals(numberOfItems + 1, numberOfItemsAfter);
    }
    @Test
    public void addUserEmailConflict() throws Exception {
        String json = "{\"userName\": \"Pavel002\", \"email\": \"test2@mail.com\", \"fullName\": \"Testovaci Uzivatel\", \"dateOfBirth\": \"1990-12-12\"}";
        int numberOfItems = usersService.getUsers(null, null, null).size();
        MvcResult prep = mockMvc.perform(
                        post("/api/v1/user")
                                .header("Content-Type", "application/json")
                                .content(json)
                )
                .andExpect(status().isOk())
                .andReturn();
        MvcResult result = mockMvc.perform(
                        post("/api/v1/user")
                                .header("Content-Type", "application/json")
                                .content(json)
                )
                .andExpect(status().isConflict())
                .andReturn();
        int numberOfItemsAfter = usersService.getUsers(null, null, null).size();
        assertEquals(numberOfItems + 1, numberOfItemsAfter);
    }
    @Test
    public void addUserInvalidDate() throws Exception {
        String json = "{ \"userName\": \"Pavlik001\", \"fullName\": \"Testovaci Uzivatel\", \"dateOfBirth\": \"1990-12-33\"}";
        int numberOfItems = usersService.getUsers(null, null, null).size();
        MvcResult result = mockMvc.perform(
                        post("/api/v1/user")
                                .header("Content-Type", "application/json")
                                .content(json)
                )
                .andExpect(status().isBadRequest())
                .andReturn();
        int numberOfItemsAfter = usersService.getUsers(null, null, null).size();
        assertEquals(numberOfItems, numberOfItemsAfter);
    }
    @Test
    public void addUserMissingUserName() throws Exception {
        String json = "{\"fullName\": \"Testovaci Uzivatel\", \"dateOfBirth\": \"1990-12-13\"}";
        int numberOfItems = usersService.getUsers(null, null, null).size();
        MvcResult result = mockMvc.perform(
                        post("/api/v1/user")
                                .header("Content-Type", "application/json")
                                .content(json)
                )
                .andExpect(status().isBadRequest())
                .andReturn();
        int numberOfItemsAfter = usersService.getUsers(null, null, null).size();
        assertEquals(numberOfItems, numberOfItemsAfter);
    }
    @Test
    public void addUserMissingFullName() throws Exception {
        String json = "{ \"userName\": \"Pavlik017\", \"dateOfBirth\": \"1990-12-13\"}";
        int numberOfItems = usersService.getUsers(null, null, null).size();
        MvcResult result = mockMvc.perform(
                        post("/api/v1/user")
                                .header("Content-Type", "application/json")
                                .content(json)
                )
                .andExpect(status().isOk())
                .andReturn();
        int numberOfItemsAfter = usersService.getUsers(null, null, null).size();
        assertEquals(numberOfItems+1, numberOfItemsAfter);
    }
    @Test
    public void addUserMissingDateOfBirth() throws Exception {
        String json = "{\"fullName\": \"Testovaci Uzivatel\", \"userName\": \"Pavlik008\"}";
        int numberOfItems = usersService.getUsers(null, null, null).size();
        MvcResult result = mockMvc.perform(
                        post("/api/v1/user")
                                .header("Content-Type", "application/json")
                                .content(json)
                )
                .andExpect(status().isOk())
                .andReturn();
        int numberOfItemsAfter = usersService.getUsers(null, null, null).size();
        assertEquals(numberOfItems+1, numberOfItemsAfter);
    }
}
